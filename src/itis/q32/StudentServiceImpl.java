package itis.q32;

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentServiceImpl implements StudentService {


    @Override
    public Group getSmallestGroup(List<Student> students) {
        Map<Group, List<Student>> collect = students.stream().collect(Collectors.groupingBy(Student::getGroup));
      return collect.entrySet().stream().min(Comparator.comparingInt(entry -> entry.getValue().size())).map(Map.Entry::getKey).get();
    }

    @Override
    public Integer countGroupsWithOnlyAdultStudents(List<Student> students) {
        LocalDate localDate = LocalDate.now();
        Map<Group, List<Student>> collect = students.stream().collect(Collectors.groupingBy(Student::getGroup));
        Long result = collect.entrySet().stream().filter(entry ->
                entry.getValue().stream().filter(student ->
                        (localDate.getYear() - student.getBirthdayDate().getYear() > 18) ||
                                (localDate.getYear() - student.getBirthdayDate().getYear() == 18 &&
                                        localDate.getMonth().getValue() - student.getBirthdayDate().getMonth().getValue() > 0) ||
                                (localDate.getYear() - student.getBirthdayDate().getYear() == 18 &&
                                        localDate.getMonth().getValue() - student.getBirthdayDate().getMonth().getValue() == 0
                                && localDate.getDayOfMonth() - student.getBirthdayDate().getDayOfMonth() >= 0)
                ).count() == entry.getValue().size()).count();
        return Integer.parseInt(String.valueOf(result));
    }

    @Override
    public Map<String, Integer> getGroupScoreSumMap(List<Student> students, String studentSurname) {
        return students
                .stream()
                .filter(student -> student.getFullName()
                        .split(" ")[0]
                        .equals(studentSurname))
                .collect(Collectors.groupingBy(student -> student.getGroup().getTitle(), Collectors.summingInt(Student::getScore)));
    }

    @Override
    public Map<Boolean, Map<String, Integer>> groupStudentScoreWithThreshold(List<Student> students, Integer threshold) {
        Map<String, Integer> collect = students.stream()
                .collect(Collectors.toMap(Student::getFullName, Student::getScore));

        return collect.keySet().stream()
                .collect(Collectors.toMap(s -> threshold - collect.get(s) > 0, o -> collect));
    }
}
