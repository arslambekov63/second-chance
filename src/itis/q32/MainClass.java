package itis.q32;


/*
В папке resources находятся два .csv файла.
Один содержит данные о группах в университете в следующем формате: ID группы, название группы, код группы
Второй содержит данные о студентах: ФИО, дата рождения, айди группы, количество очков рейтинга

напишите код который превратит содержимое файлов в обьекты из пакета "entities", выведите в консоль всех студентов,
в читабельном виде, с информацией о группе
Используя StudentService, выведите:

1. Число групп с только совершеннолетними студентами
2. Самую маленькую группу
3. Отношение группа - сумма балов студентов фамилия которых совпадает с заданной строкой
4. Отношения студент - дельта баллов до проходного порога (порог передается параметром),
 сгруппированные по признаку пройден порог, или нет

Требования к реализации: все методы в StudentService должны быть реализованы с использованием StreamApi.
Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
*/

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MainClass {

    private StudentService studentService = new StudentServiceImpl();

    public static void main(String[] args) throws IOException {
        new MainClass().run(
                "src/itis/q32/resources/students.csv",
                "src/itis/q32/resources/groups.csv");
    }

    private void run(String studentsPath, String groupsPath) throws IOException {
        File studentsFile = new File(studentsPath);
        File groupFile = new File(groupsPath);
        FileReader fileReader = new FileReader(groupFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String data = bufferedReader.readLine();
        List<Group> groups = new ArrayList<>();
        while (data != null) {
            data = data.replaceAll(" ", "");
            String[] dataSplited = data.split(",");
            Group group = new Group();
            group.setId(Long.parseLong(dataSplited[0]));
            group.setTitle(dataSplited[1]);
            group.setCode(dataSplited[2]);
            groups.add(group);
            data = bufferedReader.readLine();
        }

        bufferedReader.close();
        FileReader fileReaderStudents = new FileReader(studentsFile);
        BufferedReader bufferedReaderStudents  = new BufferedReader(fileReaderStudents);
        data = bufferedReaderStudents.readLine();
        List<Student> students = new ArrayList<>();
        while (data != null) {
            String[] dataSpl = data.split(",");
            dataSpl[1] = dataSpl[1].replaceAll(" ", "");
            dataSpl[2] = dataSpl[2].replaceAll(" ", "");
            dataSpl[3] = dataSpl[3].replaceAll(" ", "");
            Student student = new Student();
            student.setFullName(dataSpl[0]);
            student.setBirthdayDate(LocalDate.parse(dataSpl[1]));
        student.setGroup(groups
                .stream()
                .filter(group -> group.getId() == Integer.parseInt(dataSpl[2]))
                .findFirst()
                .get());
        student.setScore(Integer.parseInt(dataSpl[2]));
        students.add(student);
            data = bufferedReaderStudents.readLine();
        }
        StudentServiceImpl studentService = new StudentServiceImpl();

        System.out.println(studentService.getSmallestGroup(students).toString());

        System.out.println(studentService.countGroupsWithOnlyAdultStudents(students));

        Map<String, Integer> map = studentService.getGroupScoreSumMap(students, "Воробьянинов");
        map.forEach((key, value) -> System.out.println(key + " --> " + value));
    }

}










